#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import dbus
import os
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib

DBusGMainLoop(set_as_default=True)


def when_action_invoked(id_,action_key):
    print(id_) # int
    print(action_key) # string
    os.system("firefox&") # open firefox in background
    loop.quit() # quit loop

def quit_(*argv):
    loop.quit() # quit loop
    
def notification(msg,app_name="",replaces_id=1,app_icon="",summary="",timeout=5000,actions="",hints=""):
    bus = dbus.SessionBus()
    obj = bus.get_object("org.freedesktop.Notifications","/org/freedesktop/Notifications")
    interf = dbus.Interface(obj,"org.freedesktop.Notifications")
    
    
    serverinformation = interf.GetServerInformation()
    print("***************************")
    print("Notifications Server Information : ")
    for info in serverinformation:
        print(info)
    print("***************************\n")
    
    serverinformation = interf.GetCapabilities()
    print("***************************")
    print("Notifications Server Capabilities : ")
    for cap in serverinformation:
        print(cap)
    print("***************************\n")
    
    
    if actions:
        interf.connect_to_signal("ActionInvoked", when_action_invoked)
        #when action invoked call when_action_invoked() and send notification id and action_key.
    
    interf.connect_to_signal("NotificationClosed", quit_) # when Notifaction closed by user or timeout  call quit_ 
    
    id_ = interf.Notify(app_name,replaces_id,app_icon,summary,msg,actions,hints,timeout)
    # send and show norification and return  norification id


notification(msg="Click Here TO Open Firefox",summary="Hello",\
app_icon="call-start",actions=["Click","Open Firefox"],\
hints={"resident":True, "action-icons":True,"desktop-entry":"firefox",\
"image-path":"document-print-preview","sound-name":"network-connectivity-lost"})
#("click" is  action_key) ("Open Firefox is label) and resident to keep notification after invoked 

# app_icon https://specifications.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html
# sound-name http://0pointer.de/public/sound-naming-spec.html
# wiki https://people.gnome.org/~mccann/docs/notification-spec/notification-spec-latest.html#hints
# wiki http://www.galago-project.org/specs/notification/0.9/x408.html

loop = GLib.MainLoop()
loop.run()
